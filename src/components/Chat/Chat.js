import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { Loading } from '../Loader/Loader';
import Content from '../Content/Content';
import './Chat.css';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../Content/actions';
import { useEffect } from 'react';


const Chat = props => {
  const dispatch = useDispatch();
  const preloader = useSelector(state => state.chat.preloader);

  useEffect(()=> {
    dispatch(actions.fetchMessages(`${props.url}/messages`));
  }, [dispatch]);

  return (
    <>
      <Header />
      <main className="chat">
        {preloader
          ? <Loading />
          : <Content />}
      </main>
      <Footer />
      
  </>
  )
};

export default Chat;