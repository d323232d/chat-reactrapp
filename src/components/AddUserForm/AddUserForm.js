import React, { useRef } from 'react';
import { useSelector } from 'react-redux';
import Button from '../Content/Button/Button';
import { useDispatch } from 'react-redux';
import * as actions from '../Login/actions.users';
import { Redirect } from 'react-router-dom';

const AddUserForm = () => {
  const dispatch = useDispatch();
  const users = useSelector(state => state.users.users);
  const inputNameRef = useRef();
  const inputRoleRef = useRef();
  let newUser = null;

  const handleCancelChanges = () => {
    
  };

  const handleSaveChanges = () => {
    if (users.some(user => user.userName === inputNameRef.current.value)) return;
    newUser = {      
      userName: inputNameRef.current.value,
      role: inputRoleRef.current.value
    };
    console.log('newUser: ', Boolean(newUser));
    dispatch(actions.addUser(newUser));
  };

  return (
    <div className="container editor">
      {newUser && <Redirect exact to="/user-list"/>}
      <form className="form-user">          
        <div className="input-field">
          <input ref={inputNameRef} placeholder="User name" type="text" className="validate"/>
        </div>
        <div className="input-field">
          <input ref={inputRoleRef} placeholder="User role" type="text" className="validate"/>
        </div>          
      </form>      
      <div className="editor-buttons">
        <Button textBtn={"Back"} onClick={handleCancelChanges}/>
        <Button textBtn={"Add user"} onClick={handleSaveChanges}/>
      </div>
      
    </div>
    
  )
}

export default AddUserForm;