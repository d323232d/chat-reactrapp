export const SET_PRELOADER = "SET_PRELOADER";
export const SET_MASSAGES = "SET_MASSAGES";
export const FETCH_MESSAGES = "FETCH_MESSAGES";

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const DEL_MESSAGE = 'DEL_MESSAGE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const SHOW_MODAL = 'SHOW_MODAL';
export const CANCEL_CHANGE = 'CANCEL_CHANGE';
export const SAVE_CHANGE = 'SAVE_CHANGE';

export const LOGIN = 'LOGIN';