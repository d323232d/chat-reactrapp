import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Button from '../Button/Button';

const UsersList = () => {
  const users = useSelector(state => state.users.users);

  const [isAdd, setIsAdd] = useState(false);

  const addUserHandler = () => {
    setIsAdd(true)
    
  }

  return (
    <div className="container users-list">
      {isAdd && <Redirect exact to="/user-form"/>}
      <Button textBtn={"Add user"} onClick={addUserHandler}/>
      <table className="users-table highlight responsive-table">
        
        <thead>
          <tr>
              <th>Name</th>
              <th>Role</th>
              <th>Actions</th>
          </tr>
        </thead>

        <tbody>
        {users.map(item => {
          return (
            <tr key={item.userName}>
              <td>{item.userName}</td>
              <td>{item.role}</td>
              <td className="table-buttons">
                <Button textBtn={"Edit"}/>
                <Button textBtn={"Delete"}/>
              </td>
            </tr>
          )
        })}        
        </tbody>
      </table>
    </div>
    
  )
}

export default UsersList;