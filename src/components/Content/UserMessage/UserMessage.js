import React from 'react';
import moment from "moment";
import * as actions from '../actions.js';
import { useDispatch } from 'react-redux';

const UserMessage = (props) => {
  let message = props.message;
  const dispatch = useDispatch();

  const onHandleDelete = (e, id) => {
    e.preventDefault();    
    dispatch(actions.delMessage(id));    
  };

  const onHandleEdit = (e) => {
    e.preventDefault();
    dispatch(actions.showModal(message.id, message.text));
  }

  return (
    <li id={message.id} className="message right collection-item own-message" >
      <div className="message-intro">
        <div className="message-text">{message.text}</div>
        <div className="message-time">{moment(message.createdAt).format("HH:mm")}</div>
      </div>
      <a href="/" className="message-edit">
        <i className="material-icons"
          onClick={e => onHandleEdit(e)}
        >edit</i>
      </a>
      <a href="/" className="message-delete">
        <i
          className="material-icons"
          onClick={e => onHandleDelete(e, message.id)}
        >clear</i>
      </a>
    </li>
  )
};

export default UserMessage;