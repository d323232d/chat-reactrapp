import React from 'react';
import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../Button/Button';
import * as actions from './../actions.js';

const MessageEditor = () => {
  const dispatch = useDispatch();

  const messages = useSelector(state => state.chat.messages);
  const editMessage = useSelector(state => state.chat.editMessage);
  
  const inputRef = useRef();

  const handleCancelChanges = () => {
    dispatch(actions.cancelChange())
  }
  const handleSaveChanges = () => {
    const newMessage = inputRef.current.value;
    const messageIndex = messages.findIndex((item) => item.id === editMessage.id);
    let messageForEdit = messages.find((item) => item.id === editMessage.id);
    messageForEdit = {
      ...messageForEdit,
      text: newMessage,
      editedAt: new Date().toISOString()
    };
    let updatedMessagesList = [...messages];
    updatedMessagesList[messageIndex] = messageForEdit;
    dispatch(actions.saveChange(updatedMessagesList));
  }

  return (
    <div className="container editor">      
      <input ref={inputRef} type="text" className="edit-message-input validate" defaultValue={editMessage.text} />    
      <div className="editor-buttons">        
        <Button textBtn={"Cancel"} onClick={handleCancelChanges} />
        <Button textBtn={"Save"}  onClick={handleSaveChanges}/>
      </div>      
    </div>
  )
}

export default MessageEditor;