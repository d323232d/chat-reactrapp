import React from 'react';
import './Content.css';
import Message from './Message/Message';
import UserMessage from './UserMessage/UserMessage';
import moment from "moment";
import Textarea from "./Textarea/Textarea";
import { useSelector } from 'react-redux';
import MessageEditor from './MessageEditor/MessageEditor';

const Content = (props) => {
  
  const messages = useSelector(state => state.chat.messages);
  const editModal = useSelector(state => state.chat.editModal);

  let numberMessages = messages.length;
  let participants = new Set(messages?.map(item => item.userId)) || 0;
  let lastMessage = messages.length ? messages[messages.length - 1] : null;

  return (
    <div className="content">
      {editModal
      ? <MessageEditor/>
      : <div>
          <header className="header chat__info container">
            <ul className="left ">
              <li className="header-title">Chat name: Friends</li>
              <li className="header-users-count">{participants ? participants.size : 0}</li>
              <li className="header-messages-count">{numberMessages}</li>
            </ul>
            <div className="header-last-message-date right">{lastMessage ? moment(lastMessage.createdAt).format("DD.MM.yyyy HH:mm") : ' '}</div>
          </header>
          <div className="message-list container">
            <ul className="collection">
              <div className="messages-divider"><span>Today</span></div>
              {
                messages.map(item => {
                  return item.user === "Julia"
                    ? <UserMessage message={item} key={item.id} />
                    : <Message message={item} key={item.id} />
                })
              }
            </ul>
          </div>
          <Textarea />
        </div>
      }
      

    </div>
  )
};

export default Content;