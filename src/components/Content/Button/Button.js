import React from 'react';

const Button = ({textBtn, onClick}) => {
    
  let chooseBtnClass = btn => {
    if(btn === "Send") {
      return "message-input-button";
    }
    if(btn === "Save") {
      return "edit-message-button";
    }
    if(btn === "Cancel") {
      return "edit-message-close";
    }
    if(btn === "Log in") {
      return "login-button";
    }
    return "button"
  };
  let btnClass = chooseBtnClass(textBtn);

  return (
    <button 
      className={`${btnClass} btn waves-effect waves-light red lighten-2`} 
      type="submit" 
      name="action"
      onClick={onClick}
    >{textBtn} <i className="material-icons right">send</i>
    </button>
  )
}

export default Button;