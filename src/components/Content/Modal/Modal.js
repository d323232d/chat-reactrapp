import React from 'react';
import Button from '../Button/Button';
import { connect } from 'react-redux';
import * as actions from '../actions.js';

const Modal = (props) => {

  const editMessageId = props.editMessage.id;

  const inputRef = React.createRef();

  const onSave = () => {
    const newMessage = inputRef.current.value;
    const messageIndex = props.messages.findIndex((item) => item.id === editMessageId);
    let messageForEdit = props.messages.find((item) => item.id === editMessageId);
    messageForEdit = {
      ...messageForEdit,
      text: newMessage,
      editedAt: new Date().toISOString()
    };
    let updatedMessagesList = props.messages;
    updatedMessagesList[messageIndex] = messageForEdit;
    props.saveChange(updatedMessagesList);
  }

  return (
    <div id="modal1" className="modal edit-message-modal" >
      <div className="modal-content">
        <input ref={inputRef} type="text" className="edit-message-input validate" defaultValue={props.editMessage.text} />
      </div>
      <div className="modal-footer">
        <Button textBtn={"Cancel"} onClick={props.cancelChange} />
        <Button textBtn={"Save"} onClick={onSave} />
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    preloader: state.chat.preloader,
    editModal: state.chat.editModal,
    editMessage: state.chat.editMessage
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);



