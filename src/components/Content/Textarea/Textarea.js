import React from 'react';
import Button from '../Button/Button';

import { connect } from 'react-redux';
import * as actions from '../actions.js';

const Textarea = (props) => {

  const textRef = React.createRef();
  let newMessage;
  const getMessageText = () => {
    newMessage = textRef.current.value;
  };

  const createMessage = () => {
    let mockData = {
      "avatar": "https://img.icons8.com/cute-clipart/452/hello-kitty.png",
      "createdAt": new Date().toISOString(),
      "editedAt": '',
      "id": Date.now() + '',
      "text": newMessage,
      "user": "Julia",
      "userId": Date.now() + 'user',
      "role": "user"
    };

    if (newMessage.trim().length) {
      props.addMessage(mockData);
      textRef.current.value = '';
    }
  };
  
  return (
    <div className="message-input container input-field">
      <textarea ref={textRef} className="message-input-text materialize-textarea" id="textarea1" onChange={getMessageText}></textarea>
      <Button textBtn="Send" onClick={createMessage} />
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    preloader: state.chat.preloader
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Textarea);