import {
  SET_USERS,
  SET_IS_ADMIN,
  SET_CURRENT_USER,
  ADD_USER
} from './actions.type.users';

const initialState = {
  users: [],
  isAdmin: false,
  currentUser: null
};

// reducer
export default function usersReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USERS:
      return {
        ...state,
        users: [...action.payload]
      };
    case SET_IS_ADMIN:
      return {
        ...state,
        isAdmin: action.payload
      };
    case SET_CURRENT_USER:
      
      return {
        ...state,
        currentUser: action.payload
      };
    case ADD_USER:      
      return {
        ...state,
        users: [...state.users, action.payload]
      };

    default:
      return state;
  }
}