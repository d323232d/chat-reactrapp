import { SET_USERS, SET_IS_ADMIN, SET_CURRENT_USER, ADD_USER } from "./actions.type.users";

export const fetchUsers = (url) => async (dispatch, getState, extra) => {
  try {
    const users = await fetch(url)
      .then(res => res.json())
      .then(data => data);    
    dispatch(setUsers(users));

  } catch (error) {
    console.log('error: ', error);
  }
};

export const setUsers = (usersList) => ({
  type: SET_USERS,
  payload: usersList
});

export const setIsAdmin = (bool) => ({
  type: SET_IS_ADMIN,
  payload: bool
});

export const setCurrentUser = (userName) => ({
  type: SET_CURRENT_USER,
  payload: userName
});
export const addUser = (user) => ({
  type: ADD_USER,
  payload: user
});