import React from 'react';
import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../Content/Button/Button';
import './Login.css';
import * as actions from './actions.users.js';
import { Redirect } from 'react-router-dom';

const Login = () => {
  const dispatch = useDispatch();
  const users = useSelector(state => state.users.users);
  const currentUser = useSelector(state => state.users.currentUser);
  const isAdmin = useSelector(state => state.users.isAdmin);
  
  const inputRef = useRef('');  

  const login = () => {
    const user = inputRef.current.value;
    
    if(user && users.some(item => item.userName === user)){
      dispatch(actions.setCurrentUser(user));
    }

    const userRole = users.find(item => item.userName === user).role;    
    if(user && userRole === "admin") {
      dispatch(actions.setIsAdmin(true));
    }
  };

  return (
    <div className="row login">
      {currentUser && <Redirect exact to="chat"/>}
      {isAdmin && <Redirect exact to="users"/>}
      
      <div className="input-field col s12">
        <input ref={inputRef} type="text" className="validate"/>
      </div>
      <Button textBtn={"Log in"} onClick={login} />
    </div>
  )
}

export default Login;