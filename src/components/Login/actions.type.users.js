export const SET_USERS = "SET_USERS";
export const SET_IS_ADMIN = "SET_IS_ADMIN";
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const ADD_USER = "ADD_USER";