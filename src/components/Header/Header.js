import React from 'react';

const Header = () => {
  return (
    <>
      <div className="navbar-fixed">
        <nav>
          <div className="nav-wrapper">
            <a href="/" className="brand-logo">MyChat</a>
          </div>
        </nav>
      </div>
    </>
  )
}

export default Header;