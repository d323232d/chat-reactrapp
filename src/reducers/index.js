import { combineReducers } from "redux";
import messageReducer from "../components/Content/messages.reducer";
import usersReducer from "../components/Login/users.reducer.js";

const rootReducer = combineReducers({
  chat: messageReducer,
  users: usersReducer
});

export default rootReducer;