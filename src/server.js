import express from "express";
import http from "http";
import path from 'path';
import routes from './routes/routes.js';
import cors from 'cors';

const PORT = 3001;

const app = express();
const httpServer = http.Server(app);
app.use(cors());

app.use(express.static(path.join(process.cwd(), "public")));

app.use(express.json());

routes(app);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});

export { app, httpServer };
