const callApi = (_store) => (next) => (action) => {
  const { callApi, type, ...rest } = action;  

  if (!callApi) {
    return next(action);
  }

  next({
    ...rest,
    type: type,
  });
  
  console.log('action: ', action);

  fetch(callApi)
    .then((res) => res.json())
    .then(data => {
      console.log(data);

      next({
        ...rest,
        type: type,
        payload: {
          response: [...data],
        }
      });
    })
    .catch(e => {
      console.log(e);
      next({
        ...rest,
        type: type,
        payload: {
          error: e,
        },
      });
    })

};

export default callApi;