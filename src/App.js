import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import * as actions from './components/Login/actions.users.js';
import './App.css';
import AddUserForm from './components/AddUserForm/AddUserForm';
import Chat from './components/Chat/Chat';
import MessageEditor from './components/Content/MessageEditor/MessageEditor';
import UsersList from './components/Content/UserList/UserList';
import Login from './components/Login/Login';

const dataURL = 'http://localhost:3001';

function App() {
  const dispatch = useDispatch();
  
  const isAdmin = useSelector(state => state.users.isAdmin);
  console.log('isAdmin: ', isAdmin);

  useEffect(() => {
    dispatch(actions.fetchUsers(`${dataURL}/users`));
  }, [dispatch]);


  return (
    <div className="App">
      <Switch>       
        
        <Redirect exact from="/" to="/login" />        
        
        <Route path="/login" exact>
          <Login />
        </Route>

        <Route path="/chat" exact>
          <Chat url={dataURL} />
        </Route>

        <Route path="/users" exact>
          <UsersList />
        </Route>

        <Route path="/user-form" exact>
          <AddUserForm />
        </Route>

        <Route path="/message-editor" exact>
          <MessageEditor />
        </Route>

      </Switch>

    </div>
  )
};

export default App;
