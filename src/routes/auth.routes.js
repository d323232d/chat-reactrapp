import { Router } from 'express';
// import db from '../database.json';
import fs from 'fs';

const router = Router();

router.post('/login', (req, res, next) => {
  try {
    const { userName } = req.body;
    
    fs.readFile("./src/database.json", function (error, data) {
      if (error) throw error;
      const db = JSON.parse(data);
      const user = db.users.find( item => item.userName === userName);
      if (user) {
        console.log('req.body: ', req.body);
        res.send(req.body);
        res.redirect('/chat');
        
        next();
      }
      return res.status(200);
    });
    

  } catch (error) {
    res.status(401).send({
      error: error,
      message: 'User not found'
    })
  } finally {
    next();
  }
});

export default router;