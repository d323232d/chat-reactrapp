import { Router } from 'express';
import fs from 'fs';
// import database from '../database.json';

const router = Router();

router.get("/", (req, res) => {
  fs.readFile("./src/database.json", function (error, data) {
    const db = JSON.parse(data);
    if (error) throw error;
    return res.status(200).json(db.users);
  });
});

router.post("/", async (req, res) => {
  try {
    const newUser = req.body;
    fs.readFile("./src/database.json", function (error, data) {
      const db = JSON.parse(data);
      db.users.push(newUser);
      console.log('db: ', db);

      fs.writeFile("./src/database", JSON.stringify(db));
    });
    
  } catch (error) {
    
    console.log(error);
  }
});

export default router;