import usersRoutes from './usersRoutes.js';
import messagesRoutes from './messagesRoutes.js';
import loginRoutes from './auth.routes.js';


export default (app) => {
  app.use("/users", usersRoutes);
  app.use("/messages", messagesRoutes);
  app.use("/login", loginRoutes);
};
