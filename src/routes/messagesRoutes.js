import { Router } from 'express';
import fs from 'fs';

const router = Router();

router.get("/", (req, res) => {
  fs.readFile("./src/database.json", function (error, data) {    
    if (error) throw error;
    const db = JSON.parse(data);
    return res.status(200).json(db.messages);
  });
});

export default router;