// import { applyMiddleware, createStore } from "redux";
// import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from "./reducers";

// export const store = createStore(
//   rootReducer,
//   composeWithDevTools(
//     applyMiddleware(thunk)   
//   )
// );

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware(thunk);
  },
})